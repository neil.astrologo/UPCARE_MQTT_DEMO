import paho.mqtt.client as mqtt
import certifi


class MQTT_topics:
    # definition for default messages
    lwt_msg = "offline"
    status_msg = "active"

    def __init__(self, clientid, json_prefix=None):
        self.reply = "reply/" + clientid
        self.lwt = "status/" + clientid
        self.nodelist = "nodelist/" + clientid
        self.bump = "bump/" + clientid
        self.command = "command/" + clientid
        self.old = "test/" + clientid

        if (json_prefix is None):
            json_prefix = "jsontest/"

        self.json = json_prefix + clientid


def readCreds(filepath):
    username = ""
    password = ""
    url = ""
    port = 8883

    with open(filepath, 'r') as f:
        lines = f.read().splitlines()

    for line in lines:
        items = line.split("=")  # assume no spaces between =

        if items[0] == "username":
            username = items[1]
        elif items[0] == "password":
            password = items[1]
        elif items[0] == "broker" or items[0] == "url":
            url = items[1]

    # check if broker is of the format ssl://url.to.broker:port
    if "ssl://" in url:
        # remove ssl:// and set port
        url = url.replace("ssl://", "")
        items = url.split(":")

        url = items[0]
        port = int(items[1])

    return username, password, url, port


class basic_subscriber(mqtt.Client):
    def __init__(self, clientid, *args, **kwargs):
        json_prefix = kwargs.pop('json_prefix', None)
        username = kwargs.pop('username', None)
        password = kwargs.pop('password', None)
        broker = kwargs.pop('broker', None)
        port = kwargs.pop('port', 8883)

        super().__init__(*args, **kwargs)
        # get json prefix
        self.topics = MQTT_topics(clientid, json_prefix)

        self.message_callback_add(self.topics.json, self.handle_data)

        self.tls_set(certifi.where())
        self.username_pw_set(username, password)
        self.connect(broker, port)
        self.loop_forever()

    def on_connect(self, client, obj, flags, rc):
        print("Connection successful")
        print(f"Subscribing to {self.topics.json}")
        self.subscribe(self.topics.json)

    def on_subscribe(self, client, userdata, mid, granted_qos):
        print("Subscription successful")

    def handle_data(self, client, obj, message):
        print(message.payload.decode("utf-8"))
