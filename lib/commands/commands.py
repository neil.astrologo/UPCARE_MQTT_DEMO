import json
import typing

def makeCommand(cmd: str, *args):
    j = {"command" : cmd}

    # check if args contains anything
    if(len(args)):
        j["payload"] = {}
        args_list = []
        i = 0
        for arg in args:
            if i == 0:
                j["payload"]["target"] = arg

            args_list.append(arg)
            i += 1

        j["payload"]["args"] = args_list

    return json.dumps(j, indent=4)