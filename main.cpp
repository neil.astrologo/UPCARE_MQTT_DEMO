#include <iostream>
#include <sstream>
#include <string>
#include <chrono>
#include <thread>
#include "mqtt/async_client.h"

class mqtt_callbacks : public virtual mqtt::callback{
	mqtt::async_client& client;
	int& ctr;

	void connected(const std::string& cause){
		if(!cause.empty()){
			std::cout << "Connected to server with cause: " << cause << std::endl;
		}

		client.subscribe("demo/datatopic", 1);
	}

	void delivery_complete(mqtt::delivery_token_ptr tok) override{
		std::cout << "Delivery complete for token " << (tok ? tok->get_message_id() : -1) << std::endl;
		std::cout << "Message number " << ctr++ << std::endl;
	}

	void message_arrived(mqtt::const_message_ptr msg) override{
		std::cout << "Received message: " << msg->to_string() << std::endl;
	}

public:
	mqtt_callbacks(mqtt::async_client& client, int& ctr) : client(client), ctr(ctr){}
};

int main(void){
	mqtt::token_ptr CONNTOK;
	std::string user = "mqttdemo";
	std::string pw = "MQTTcare2023";

	std::string broker = "ssl://ed7632329e6e4fbcbe77b1fa917585a1.s1.eu.hivemq.cloud:8883";

	mqtt::async_client client(broker, "DemoDevice");

	int ctr = 0;

	mqtt_callbacks c(client, ctr);
	client.set_callback(c);

	auto sslOpts = mqtt::ssl_options_builder().error_handler([](const std::string& msg){
			std::cerr << "SSL Error: " << msg;
		})
		.finalize();

	auto connOpts = mqtt::connect_options_builder()
		.user_name(user)
		.password(pw)
		.keep_alive_interval(std::chrono::seconds(60))
		.automatic_reconnect(true)
		.clean_session(true)
		.ssl(std::move(sslOpts))
		.finalize();

	CONNTOK = client.connect(connOpts);
	CONNTOK->wait();
	std::cout << "Connected to MQTT!" << std::endl;

	int loop_ctr = 0;
	std::cout << "Demonstrating basic publish loop" << std::endl;
    /*
	while(ctr < 5){
		std::stringstream ss;
		ss << "Message no. " << ctr;
		auto message = mqtt::make_message("demo/datatopic", ss.str(), 1, false);
		client.publish(message);
		std::cout << loop_ctr++ << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
    */

	ctr = 0;
	loop_ctr = 0;

	std::cout << "Demonstrating some improper handling of asynchronous behavior" << std::endl;
	while(ctr < 5){
		if(loop_ctr < 3){
			std::stringstream ss;
			ss << "Loop 2 message no. " << ctr;
			auto message = mqtt::make_message("demo/datatopic", ss.str(), 1, false);
			client.publish(message);
			std::cout << loop_ctr++ << std::endl;
		 } else{
		 	std::this_thread::sleep_for(std::chrono::seconds(1));
		 	loop_ctr = 0;
		 }

	}

	std::this_thread::sleep_for(std::chrono::seconds(1));
	return 0;
}
