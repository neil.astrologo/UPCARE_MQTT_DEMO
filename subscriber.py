from lib.mqtt_utils import mqtt_utils

class mod_subscriber(mqtt_utils.basic_subscriber):
    def handle_data(self, client, obj, message):
        msg = message.payload.decode("utf-8")
        print(msg)

        # get message number
        split = msg.split()

        print(split)

        if(len(split) == 3 and split[2] == '4'):
            client.publish(self.topics.json, "Hello publisher")

if __name__ == "__main__":
    sub = mod_subscriber("datatopic", json_prefix="demo/", username="mqttdemo", password="MQTTcare2023", broker="ed7632329e6e4fbcbe77b1fa917585a1.s1.eu.hivemq.cloud")
